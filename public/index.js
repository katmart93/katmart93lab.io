// clock

const time = document.querySelector('.time');
const date = document.querySelector('.date');

/**
 * @param {Date} date
 */
const formatTime = (date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return `${hours.toString().padStart(2, '0')}:${minutes
    .toString()
    .padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
};

/**
 * @param {Date} date
 */
const formatDate = (date) => {
  const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return `${days[date.getDay()]}, ${
    months[date.getMonth()]
  } ${date.getDate()} ${date.getFullYear()}`;
};

setInterval(() => {
  const now = new Date();

  time.textContent = formatTime(now);
  date.textContent = formatDate(now);
}, 200);

// programming jokes

fetch('https://official-joke-api.appspot.com/jokes/programming/random')
  .then((response) => response.json())
  .then((response) => {
    let jokeSetup = document.querySelector('.joke-setup');
    let jokePunchline = document.querySelector('.joke-punchline');

    jokeSetup.textContent = response[0].setup;
    jokePunchline.textContent = response[0].punchline;
  });
